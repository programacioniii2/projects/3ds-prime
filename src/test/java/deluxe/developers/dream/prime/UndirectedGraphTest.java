package deluxe.developers.dream.prime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UndirectedGraphTest {

  public UndirectedGraph graph;
  public Node node;
  public Node node2;
  public Node nodeA;
  public Node nodeB;
  public Node nodeC;
  public Node nodeD;
  public Node nodeE;

  @BeforeEach
  public void setUp() {
    graph = new UndirectedGraph();
    node = new Node("1");
    node2 = new Node("2");
    nodeA = new Node("A");
    nodeB = new Node("B");
    nodeC = new Node("C");
    nodeD = new Node("D");
    nodeE = new Node("E");
    graph.addNode(node);
    graph.addNode(nodeA);
    graph.addNode(nodeB);
    graph.addNode(nodeC);
    graph.addNode(nodeD);
    graph.addNode(nodeE);
  }

  @Test
  public void testAddNode() {
    assertTrue(graph.addNode(node2));
  }

  @Test
  public void testAddNodeIfNodeIsNull() {
    assertFalse(graph.addNode(null));
  }

  @Test
  public void testAddNodeIfNodeIsAlreadyAddedIntoGraph() {
    assertFalse(graph.addNode(node));
  }

  @Test
  public void testAddNodeMethodWithMultipleNodes() {
    assertTrue(graph.addNode(node2));
    assertTrue(graph.addNode(new Node("3")));
    assertTrue(graph.addNode(new Node("4")));
  }

  @Test
  public void testAddEdge() {
    graph.addNode(node2);
    assertTrue(graph.addEdge(new Edge(0, node, node2)));
  }

  @Test
  public void testAddEdgeIfDestinyNodeIsNull() {
    assertFalse(graph.addEdge(new Edge(0, node, null)));
  }

  @Test
  public void testAddEdgeIfStartNodeIsNull() {
    assertFalse(graph.addEdge(new Edge(0, null, node)));
  }

  @Test
  public void testAddEdgeIfTheEdgeAlreadyExists() {
    graph.addEdge(new Edge(0, node2, node));
    assertFalse(graph.addEdge(new Edge(0, node2, node)));
  }

  @Test
  public void testAddEdgeIfStartAndDestinyAreNull() {
    assertFalse(graph.addEdge(new Edge(0, null, null)));
  }

  @Test
  public void testAddEdgeMethodWithMultipleEdges() {
    graph.addNode(nodeA);
    graph.addNode(node2);
    graph.addEdge(new Edge(0, nodeA, node));
    graph.addEdge(new Edge(1, node2, node));
  }

  @Test
  public void testGetNodes() {
    Set<Node> set = new HashSet<>();
    set.add(node);
    set.add(nodeA);
    set.add(nodeB);
    set.add(nodeC);
    set.add(nodeD);
    set.add(nodeE);
    assertEquals(set, graph.getNodes());
  }

  @Test
  public void testKruskal() {
    graph.addEdge(new Edge(5, nodeA, nodeB));
    graph.addEdge(new Edge(4, nodeB, nodeC));
    graph.addEdge(new Edge(3, nodeC, nodeD));
    graph.addEdge(new Edge(2, nodeD, nodeE));
    graph.addEdge(new Edge(1, nodeE, nodeA));
    graph.addEdge(new Edge(3, nodeE, nodeC));

    ArrayList<Edge> arrayList = new ArrayList<>();
    arrayList.add(new Edge(1, nodeE, nodeA));
    arrayList.add(new Edge(2, nodeD, nodeE));
    arrayList.add(new Edge(3, nodeE, nodeC));
    arrayList.add(new Edge(4, nodeB, nodeC));

    for (int i = 0; i < graph.kruskal().size(); i++) {
      assertEquals(arrayList.get(i).getStartNode().getName(), graph.kruskal().get(i).getStartNode().getName());
      assertEquals(arrayList.get(i).getDestinyNode().getName(), graph.kruskal().get(i).getDestinyNode().getName());
    }
  }

  @AfterEach
  public void tearDown() {
    graph = null;
    node = null;
    node2 = null;
    nodeA = null;
    nodeB = null;
    nodeC = null;
    nodeD = null;
    nodeE = null;
  }
}
