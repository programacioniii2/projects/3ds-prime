package deluxe.developers.dream.prime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EdgeTest {

  public Node node;
  public Node node1;
  public Edge edge;

  @BeforeEach
  public void setUp() {
    node = new Node("A");
    node1 = new Node("B");
    edge = new Edge(0, node, node1);
  }

  
  @Test
  public void testGetDestinyNode() {
    assertEquals(node1, edge.getDestinyNode());
  }
  
  @Test
  public void testGetNodes() {
    assertEquals(new ArrayList<>(Arrays.asList(node, node1)), edge.getNodes());
  }

  @Test
  public void testGetStartNode() {
    assertEquals(node, edge.getStartNode());
  }
  
  @Test
  public void testGetWeight() {
    assertEquals(0, edge.getWeight());
  }
  
  @AfterEach
  public void tearDown() {
    node = null;
    node1= null;
    edge = null;
  }
}
