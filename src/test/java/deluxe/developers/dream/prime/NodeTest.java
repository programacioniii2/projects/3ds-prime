package deluxe.developers.dream.prime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NodeTest {

  public Node node;
  public Node node1;

  @BeforeEach
  public void setUp() {
    node = new Node("Name");
    node1 = new Node(null);
  }

  @Test
  public void testGetName() {
    assertEquals("Name", node.getName());
  }
  
  @Test
  public void testGetNameIfNameIsNull() {
    assertNull(node1.getName());
  }

  @AfterEach
  public void tearDown() {
    node = null;
    node1 = null;
  }
}
