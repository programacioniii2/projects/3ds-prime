package deluxe.developers.dream.prime;

import java.util.ArrayList;

public interface IKruskal {
  public ArrayList<Edge> kruskal();
}
