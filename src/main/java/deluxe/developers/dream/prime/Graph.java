package deluxe.developers.dream.prime;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class Graph {

  protected final Set<Node> nodes;
  protected final ArrayList<Edge> edges;

  public Graph() {
    this.nodes = new HashSet<>();
    this.edges = new ArrayList<>();
  }

  public Set<Node> getNodes() {
    return nodes;
  }

  public ArrayList<Edge> getEdges() {
    return edges;
  }

  public boolean addNode(Node node) {

    if (node == null || nodes.contains(node)) {
      return false;
    }
    nodes.add(node);
    return true;
  }

  public boolean addEdge(Edge edge) {
    boolean edgeIsNull = edge.equals(null);
    boolean edgeContains = edges.contains(edge) || !nodes.contains(edge.getDestinyNode())
        || !nodes.contains(edge.getStartNode());
    if (edgeIsNull || edgeContains) {
      return false;
    }
    if (edges.isEmpty()) {
      return edges.add(edge);
    }
    for (Edge edgeIteration : edges) {
      if (edge.getWeight() <= edgeIteration.getWeight()) {
        edges.add(edges.indexOf(edgeIteration), edge);
        return true;
      }
    }

    return edges.add(edge);
  }

  protected boolean bfsSearch(Node start, Node destiny, ArrayList<Edge> edges) {
    return bfsSearch(start, destiny, new ArrayList<>(), new ArrayDeque<>(), edges);
  }

  private boolean bfsSearch(Node nodeA, Node destiny, ArrayList<Node> visited, ArrayDeque<Node> nodes,
      ArrayList<Edge> edgesAvailable) {
    visited.add(nodeA);
    for (Edge edge : edgesAvailable) {
      if (edge.getStartNode().equals(nodeA)) {
        if (edge.getDestinyNode().equals(destiny)) {
          return true;
        }
        visited.add(edge.getDestinyNode());
        nodes.add(edge.getDestinyNode());
      }
    }
    if (nodes.size() == 0) {
      return false;
    }
    return bfsSearch(nodes.poll(), destiny, visited, nodes, edgesAvailable);
  }
}
