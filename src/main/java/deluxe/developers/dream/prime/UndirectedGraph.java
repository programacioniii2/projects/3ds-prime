package deluxe.developers.dream.prime;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class UndirectedGraph extends Graph implements IKruskal {

  @Override
  public ArrayList<Edge> kruskal() {
    ArrayList<Graph> forest = new ArrayList<Graph>();
    for (Node node : this.nodes) {
      Graph newGraph = new UndirectedGraph();
      newGraph.addNode(node);
      forest.add(newGraph);
    }
    return kruskal(new ArrayDeque<>(edges), forest, new ArrayList<Edge>());
  }

  private ArrayList<Edge> kruskal(ArrayDeque<Edge> edges, ArrayList<Graph> forest, ArrayList<Edge> minimumSpaningTree) {
    if (edges.isEmpty() && forest.size() > 1) {
      return minimumSpaningTree;
    }
    for (Graph graph : forest) {
      if (graph.getNodes().contains(edges.peek().getStartNode())) {
        for (Graph graph1 : forest) {
          if (graph1.getNodes().contains(edges.peek().getDestinyNode()) && graph1 != graph) {
            for (Node node : graph1.getNodes()) {
              graph.addNode(node);
            }
            forest.remove(graph1);

            minimumSpaningTree.add(edges.poll());
            return kruskal(edges, forest, minimumSpaningTree);
          }
        }
      }
    }
    edges.poll();
    return kruskal(edges, forest, minimumSpaningTree);
  }
}
