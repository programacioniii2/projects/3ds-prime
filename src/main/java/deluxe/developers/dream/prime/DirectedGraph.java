package deluxe.developers.dream.prime;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;

public class DirectedGraph extends Graph implements IKruskal {

  DirectedGraph() {
    super();
  }

  @Override
  public ArrayList<Edge> kruskal() {
    return kruskal(new ArrayDeque<>(edges), new ArrayList<Edge>(), new HashSet<Node>());
  }

  private ArrayList<Edge> kruskal(ArrayDeque<Edge> sortedEdges, ArrayList<Edge> result, HashSet<Node> nodes) {
    if (sortedEdges.isEmpty()) {
      return result;
    }
    ArrayList<Edge> aux = new ArrayList<>(result);
    aux.add(sortedEdges.peek());
    if (bfsSearch(sortedEdges.peek().getStartNode(), sortedEdges.peek().getStartNode(), aux)) {
      sortedEdges.remove();
      return kruskal(sortedEdges, result, nodes);
    }
    nodes.add(sortedEdges.peek().getStartNode());
    nodes.add(sortedEdges.peek().getDestinyNode());
    result.add(sortedEdges.poll());
    return kruskal(sortedEdges, result, nodes);
  }
}
