package deluxe.developers.dream.prime;

import java.util.ArrayList;

public class Edge {

  private final int weight;
  private final Node startNode;
  private final Node destinyNode;
  private final ArrayList<Node> nodes;

  public Edge(int weight, Node startNode, Node destinyNode) {
    this.weight = weight;
    this.startNode = startNode;
    this.destinyNode = destinyNode;
    this.nodes = new ArrayList<Node>(2);
    nodes.add(startNode);
    nodes.add(destinyNode);
  }

  public int getWeight() {
    return weight;
  }

  public Node getStartNode() {
    return startNode;
  }

  public Node getDestinyNode() {
    return destinyNode;
  }

  public ArrayList<Node> getNodes() {
    return nodes;
  }

}
